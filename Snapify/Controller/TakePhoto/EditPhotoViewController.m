//
//  EditPhotoViewController.m
//  Snapify (from original AnyPic)
//
//  Modified by Christopher Coudriet on 12/31/13 (Created by Héctor Ramos on 5/04/12)
//  Copyright (c) 2013 Christopher Coudriet / 2012 Héctor Ramos. All rights reserved.
//

#import "EditPhotoViewController.h"
#import "PhotoDetailsFooterView.h"
#import "UIImage+ResizeAdditions.h"
#import "AppDelegate.h"

@interface EditPhotoViewController ()

#pragma mark - Properties
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UITextField *commentTextField;
@property (nonatomic, strong) PFFile *photoFile;
@property (nonatomic, strong) PFFile *thumbnailFile;
@property (nonatomic, assign) UIBackgroundTaskIdentifier fileUploadBackgroundTaskId;
@property (nonatomic, assign) UIBackgroundTaskIdentifier photoPostBackgroundTaskId;

@end

@implementation EditPhotoViewController

#pragma mark - Instance Methods

- (id)initWithImage:(UIImage *)aImage
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        if (!aImage) {
            return nil;
        }
        
        self.image = aImage;
        self.editImagePub = aImage;
        self.fileUploadBackgroundTaskId = UIBackgroundTaskInvalid;
        self.photoPostBackgroundTaskId = UIBackgroundTaskInvalid;
    }
    
    return self;
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    NSLog(@"Memory warning on Edit");
}

#pragma mark - Aviary Delegate

- (void)displayEditorForImage:(UIImage *)imageToEdit
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [AFPhotoEditorController setAPIKey:@"43cbeb9d24693c8f" secret:@"99f199477794a61d"];
    });

    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:editImagePub];
    
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:nil];
}

- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)imageEdited
{
    [fotoView setImage:imageEdited];
    editImagePub = imageEdited;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavBar];
    [self configureView];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self doneButtonAction:textField];
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.commentTextField resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)note
{
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGSize scrollViewContentSize = self.scrollView.bounds.size;
    scrollViewContentSize.height += keyboardFrameEnd.size.height;
    [self.scrollView setContentSize:scrollViewContentSize];
    
    CGPoint scrollViewContentOffset = self.scrollView.contentOffset;
    // Align the bottom edge of the photo with the keyboard
    scrollViewContentOffset.y = scrollViewContentOffset.y + keyboardFrameEnd.size.height*3.0f - [UIScreen mainScreen].bounds.size.height+43.0f;
    
    [self.scrollView setContentOffset:scrollViewContentOffset animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    CGRect keyboardFrameEnd = [[note.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGSize scrollViewContentSize = self.scrollView.bounds.size;
    scrollViewContentSize.height -= keyboardFrameEnd.size.height;
    [UIView animateWithDuration:0.200f animations:^{
        [self.scrollView setContentSize:scrollViewContentSize];
    }];
}

#pragma mark - Private Methods

- (void)configureNavBar
{
    self.navigationItem.title = @"TinApp";
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonAction:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Publish" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonAction:)];
 
}

- (void)configureView
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    parseAdminCreds = [appDelegate.adminData objectForKey:@"adminCreds"];
    
    if (parseAdminCreds.count > 0)
    {
        adminObjectId = [parseAdminCreds objectAtIndex:0];
        NSLog(@"adminObjectId = %@", adminObjectId);
        
        adminPushChannel = [parseAdminCreds objectAtIndex:1];
        NSLog(@"adminPushChannel = %@", adminPushChannel);
    }
    else
    {
        adminObjectId = @"Not Found!";
        NSLog(@"adminObjectId = %@", adminObjectId);
        
        adminPushChannel = @"Not Found!";
        NSLog(@"adminPushChannel = %@", adminPushChannel);
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 320.0f, 320.0f, 500.0f)];
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.delegate = self;
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.view = self.scrollView;
    
    fotoView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 320.0f)];
    [fotoView setBackgroundColor:[UIColor blackColor]];
    [fotoView setImage:self.editImagePub];
    [fotoView setContentMode:UIViewContentModeScaleAspectFit];
    
    CALayer *layer = fotoView.layer;
    layer.masksToBounds = NO;
    layer.shadowRadius = 3.0f;
    layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    layer.shadowOpacity = 0.25f;
    layer.shouldRasterize = YES;
    
    [self.scrollView addSubview:fotoView];
    
    UIButton *openEditor = [[UIButton alloc] initWithFrame:CGRectMake(0, 320, 320, 40)];
    [openEditor setBackgroundColor:[UIColor grayColor]];
    [openEditor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [openEditor setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [openEditor setTitle:@"Open in Photo Editor" forState:UIControlStateNormal];
    [openEditor addTarget:self action:@selector(displayEditorForImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:openEditor];
    
    CGRect footerRect = [PhotoDetailsFooterView rectForView];
    footerRect.origin.y = fotoView.frame.origin.y + fotoView.frame.size.height +40;
    
    PhotoDetailsFooterView *footerView = [[PhotoDetailsFooterView alloc] initWithFrame:footerRect];
    self.commentTextField = footerView.commentField;
    self.commentTextField.delegate = self;
    [self.scrollView addSubview:footerView];
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width, fotoView.frame.origin.y + fotoView.frame.size.height + footerView.frame.size.height)];

}

- (void)doneButtonAction:(id)sender
{
    NSLog(@"Comment - %@",self.commentTextField.text);
    
    if (_commentTextField.text.length < 1){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please add a comment before publishing your photo." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
        return;
    }
    
    [TAPINetworkController shouldUploadImage:_image photoFile:self.photoFile thumbnailFile:self.thumbnailFile task:self.fileUploadBackgroundTaskId editedImage:self.editImagePub];
    
    NSDictionary *userInfo = [NSDictionary dictionary];
    NSString *trimmedComment = [self.commentTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (trimmedComment.length != 0) {
        userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                    trimmedComment,kPAPEditPhotoViewControllerUserInfoCommentKey,
                    nil];
    }
    
    if (!self.photoFile || !self.thumbnailFile) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Something went wrong. Could not post your photo. Please try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    [TAPINetworkController didPostItem:self.photoFile thumbnailFile:self.thumbnailFile task:self.photoPostBackgroundTaskId adminID:self.adminObjectId info:userInfo];
       
    [self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (void)cancelButtonAction:(id)sender
{
    [self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
@end