//
//  TAPINetworkController.h
//  Tin
//
//  Created by Eric Partyka on 10/28/14.
//  Copyright (c) 2014 Christopher Coudriet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TAPINetworkController : NSObject

+(BOOL)shouldUploadImage:(UIImage *)anImage photoFile:(PFFile *)photoFile thumbnailFile:(PFFile *)thumbnailFile task:(UIBackgroundTaskIdentifier)task editedImage:(UIImage *)editedImage;

+(void)didPostItem:(PFFile *)photoFile thumbnailFile:(PFFile *)thumbnailFile task:(UIBackgroundTaskIdentifier)task adminID:(NSString *)adminID info:(NSDictionary *)info;


@end
